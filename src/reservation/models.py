from django.db import models
from django.utils import timezone
from datetime import date
from django.conf import settings
from django.contrib.auth.models import User


class State(models.Model):
    name = models.CharField(max_length=24, blank=False, null=False)

    def __str__(self):
        return self.name

    class Meta:
        ordering = ('name',)


class Hotel(models.Model):
    name = models.CharField(max_length=24, blank=False, null=False)
    address = models.CharField(max_length=255, blank=False, null=False, unique=True)
    rank = models.IntegerField(blank=True, null=True)
    state = models.ForeignKey('State', blank=False, null=True, on_delete=models.SET_NULL)


    def __str__(self):
        return self.name

    class Meta:
        ordering = ('rank',)


class PhoneNumber(models.Model):
    hotel = models.ForeignKey('Hotel', blank=True, on_delete=models.CASCADE)
    phoneNumber = models.CharField(max_length=13, blank=False, null=False)

    def __str__(self):
        return self.phoneNumber


class Room(models.Model):
    hotel = models.ForeignKey('Hotel', blank=True, on_delete=models.CASCADE)
    number = models.IntegerField(blank=False, null=False)
    maxOccupation = models.IntegerField(blank=True, null=False)
    bedQuantity = models.IntegerField(blank=True, null=False)
    price = models.IntegerField(blank=True, null=False)

    def __str__(self):
        return self.number


class Service(models.Model):
    serviceName = models.CharField(max_length=64, blank=False, null=False)

    def __str__(self):
        return self.serviceName


class RoomService(models.Model):
    room = models.ManyToManyField(Room)
    service = models.ManyToManyField(Service)
#
# class User(models.Model):
#     email = models.EmailField()
#     password = models.CharField(max_length=64, blank=False, null=False)
#     role = models.CharField(max_length=64, blank=True, null=True)
#     def __str__(self):
#         return self.email


class Customer(models.Model):
    user = models.ForeignKey('auth.User', related_name='customers', on_delete=models.CASCADE)
    customerName = models.CharField(max_length=64, blank=False, null=False)
    customerPhoneNumber = models.CharField(max_length=13, blank=True, null=True)
    customerAddress = models.CharField(max_length=256, blank=True, null=True)

    def __str__(self):
        return self.customerName


class Reservation(models.Model):
    total = models.IntegerField(blank=False, null=False)
    start =  models.DateField(auto_now=False, auto_now_add=False)
    end =  models.DateField(auto_now=False, auto_now_add=False)
    timestamp = models.DateTimeField(auto_now=True)
    customer = models.ForeignKey('Customer', blank=True, null=True, on_delete=models.DO_NOTHING)
    rooms = models.CharField(max_length=2000)
    occupation = models.IntegerField(blank=True, null=True)
    # rooms = models.ForeignKey('Room', related_name='rooms', blank=False, null=False, on_delete=models.DO_NOTHING)
    def __str__(self):
        return self.startDate, self.endDate


class RoomReservation(models.Model):
    occupation = models.IntegerField(blank=False, null=False)
    room = models.ManyToManyField(Room)
    reservation = models.ManyToManyField(Reservation)

    def __str__(self):
        return self.occupation
