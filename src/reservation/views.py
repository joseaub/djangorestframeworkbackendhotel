from django.shortcuts import render, get_object_or_404
from django.contrib.auth.models import User, Group
from rest_framework import viewsets, generics, status, mixins
from src.reservation.serializers import (UserSerializer, GroupSerializer,
    CustomerSerializer, CustomerCreateSerializer,
    PhoneNumberSerializer, PhoneNumberCreateSerializer,
    HotelSerializer, HotelCreateSerializer, HotelPhonesSerializer,
    StateSerializer, StateCreateSerializer,
    ReservationSerializer, ReservationCreateSerializer,
    RoomSerializer, RoomCreateSerializer,
    RoomReservationCreateSerializer, RoomReservationSerializer)
from .models import Customer, State, PhoneNumber, Hotel, Reservation, Room, RoomReservation
from rest_framework.response import Response
from rest_framework.request import Request


class UserViewSet(viewsets.ModelViewSet):
    """
    API endpoint that allows users to be viewed or edited.
    """
    serializer_class = UserSerializer

    def get_queryset(self):
        return User.objects.all()


class GroupViewSet(viewsets.ModelViewSet):
    """
    API endpoint that allows groups to be viewed or edited.
    """
    queryset = Group.objects.all()
    serializer_class = GroupSerializer


class CustomerViewSet(viewsets.ModelViewSet):
    """
    API endpoint that allows customers to be viewed or edited.
    """
    serializer_class = CustomerSerializer

    def get_queryset(self):
        return Customer.objects.all()

    def list(self, request):
        # Note the use of `get_queryset()` instead of `self.queryset`
        # data1 = request.query_params
        # print(data1.get('data'))

        # data2 = request.data
        # print(data2)
        queryset = self.get_queryset()
        serializer = CustomerSerializer(queryset, context={'request': request}, many=True).data
        # print(serializer)
        return Response(serializer)

    def create(self, request):
        serializer = CustomerSerializer(data=request.data, context={'request': request})
        valid_serializer = CustomerCreateSerializer(data=request.data)

        if valid_serializer.is_valid():
            # valid_serializer.save()
            return Response(valid_serializer.data, status=status.HTTP_201_CREATED)
        return Response(valid_serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    def perform_create(self, serializer):
        serializer.save(user=self.request.user)

    def retrieve(self, request, pk=None):
        queryset = Customer.objects.filter(pk=pk)[0]
        serializer = CustomerSerializer(queryset).data
        # print(serializer)
        return Response(serializer)

    def update(self, request, pk, format=None):
        queryset = Customer.objects.filter(pk=pk)[0]
        serializer = CustomerSerializer(queryset, data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    # def partial_update(self, request, pk=None):
    #     pass

    def destroy(self, request, pk=None):
        queryset = Customer.objects.filter(pk=pk)[0]
        queryset.delete()
        return Response(status=status.HTTP_204_NO_CONTENT)


class StateViewSet(viewsets.ModelViewSet):
    serializer_class = StateSerializer

    def get_queryset(self):
        return State.objects.all()

    def create(self, request):
        valid_serializer = StateCreateSerializer(data=request.data)
        if valid_serializer.is_valid():
            valid_serializer.saveState()
            return Response(valid_serializer.data, status=status.HTTP_201_CREATED)
        return Response(valid_serializer.errors, status=status.HTTP_400_BAD_REQUEST)


class StateList(generics.ListCreateAPIView):
    queryset = State.objects.all()
    serializer_class = StateSerializer
    # permission_classes = (permissions.IsAuthenticatedOrReadOnly,
    #                       IsOwnerOrReadOnly,)

    #
    # def get(self, request, *args, **kwargs):
    #     return self.list(request, *args, **kwargs)
    #
    # def post(self, request, *args, **kwargs):
    #     return self.create(request, *args, **kwargs)


class PhoneNumberViewSet(viewsets.ModelViewSet):
    serializer_class = PhoneNumberSerializer

    def get_queryset(self):
        return PhoneNumber.objects.all()

    def list(self, request):
        # phones = PhoneNumber.objects.all()
        hotels = Hotel.objects.all()
        response = dict()
        response['status'] = True
        response['code'] = 350
        # response['data'] = PhoneNumberSerializer(phones, many=True).data
        response['data'] = HotelPhonesSerializer(hotels, many=True).data
        return Response(response)

    def create(self, request):
        valid_serializer = PhoneNumberCreateSerializer(data=request.data)
        if valid_serializer.is_valid():
            valid_serializer.savePhoneNumber()
            return Response(valid_serializer.data, status=status.HTTP_201_CREATED)
        return Response(valid_serializer.errors, status=status.HTTP_400_BAD_REQUEST)


class HotelViewSet(viewsets.ModelViewSet):
    serializer_class = HotelSerializer

    def get_queryset(self):
        return Hotel.objects.all()

    def create(self, request):
        valid_serializer = HotelCreateSerializer(data=request.data)
        if valid_serializer.is_valid():
            valid_serializer.saveHotel()
            return Response(valid_serializer.data, status=status.HTTP_201_CREATED)
        return Response(valid_serializer.errors, status=status.HTTP_400_BAD_REQUEST)


class ReservationViewSet(viewsets.ModelViewSet):
    serializer_class = ReservationSerializer

    def get_queryset(self):
        return Reservation.objects.all()

    def create(self, request):
        valid_serializer = ReservationCreateSerializer(data=request.data)
        if valid_serializer.is_valid():
            valid_serializer.saveReservation()
            return Response(valid_serializer.data, status=status.HTTP_201_CREATED)
        return Response(valid_serializer.errors, status=status.HTTP_400_BAD_REQUEST)


class RoomViewSet(viewsets.ModelViewSet):
    serializer_class = RoomSerializer

    def get_queryset(self):
        return Room.objects.all()

    def create(self, request):
        valid_serializer = RoomCreateSerializer(data=request.data)
        if valid_serializer.is_valid():
            valid_serializer.saveRoom(request.data)
            return Response(valid_serializer.data, status=status.HTTP_201_CREATED)
        return Response(valid_serializer.errors, status=status.HTTP_400_BAD_REQUEST)

class RoomReservationViewSet(viewsets.ModelViewSet):
    serializer_class = RoomReservationSerializer

    def get_queryset(self):
        return RoomReservation.objects.all()

    def create(self, request):
        valid_serializer = RoomReservationCreateSerializer(data=request.data)
        if valid_serializer.is_valid():
            valid_serializer.saveRoomReservation()
            return Response(valid_serializer.data, status=status.HTTP_201_CREATED)
        return Response(valid_serializer.errors, status=status.HTTP_400_BAD_REQUEST)
