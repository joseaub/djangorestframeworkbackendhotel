from django.contrib.auth.models import User, Group
from rest_framework import serializers
from rest_framework.relations import PrimaryKeyRelatedField
from .models import Customer, State, PhoneNumber, Hotel, Reservation, Room, RoomReservation
import datetime

class UserSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = User
        fields = ('url', 'username', 'email', 'groups')
        customers = serializers.PrimaryKeyRelatedField(queryset=Customer.objects.all())


class GroupSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Group
        fields = ('url', 'name')


class CustomerSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Customer
        fields = ('user', 'id', 'customerName', 'customerAddress', 'customerPhoneNumber')


class CustomerCreateSerializer(serializers.Serializer):
    customerName = serializers.CharField()
    customerAddress = serializers.CharField()

    # def validate_name(self, value):
    #     print(value)
    #     raise serializers.ValidationError("este es un error")
    #
    # def validate_address(self, value):
    #     return value


class StateSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = State
        fields = ('id', 'name')
        hotels = serializers.PrimaryKeyRelatedField(queryset=Hotel.objects.all())


class StateCreateSerializer(serializers.Serializer):
    name = serializers.CharField()

    def validate_name(self, value):
        name = value
        name_in_db = State.objects.filter(name=name)
        if name_in_db.exists():
            raise serializers.ValidationError('That place is alredy registered')
        return value

    def saveState(self):
        name = self.validated_data.get("name")
        instances = State(name=name)
        instances.save()
        return True


class PhoneNumberInitSerializer(serializers.Serializer):
    phoneNumber = serializers.CharField()


class HotelCreateSerializer(serializers.Serializer):
    address = serializers.CharField()
    name = serializers.CharField()
    state = serializers.IntegerField()

    def validate_address(self, value):
        address = value
        address_in_db = Hotel.objects.filter(address=address)
        if address_in_db.exists():
            raise serializers.ValidationError('This address is already taken')
        return value

    def validate_name(self, value):
        return value

    def validate_state(self, value):
        state = value
        state_in_db = State.objects.filter(pk=state)
        if not state_in_db.exists():
            raise serializers.ValidationError('Unknown state')
        return value

    def saveHotel(self):
        state = self.validated_data.get("state")
        name = self.validated_data.get("name")
        address = self.validated_data.get("address")
        state_in_db = State.objects.filter(pk=state).first()
        instances = Hotel(address=address, state_id=state_in_db.pk, name=name)
        instances.save()
        return True


class HotelSerializer(serializers.Serializer):
    name = serializers.CharField()
    address = serializers.CharField()
    state = serializers.CharField()
    phone_list = serializers.SerializerMethodField()

    def get_phone_list(self, object):
        phone_list = PhoneNumber.objects.filter(hotel_id=object.pk)
        return PhoneNumberInitSerializer(phone_list, many=True).data


class HotelPhonesSerializer(serializers.Serializer):
    name = serializers.CharField()
    phone_list = serializers.SerializerMethodField()
    phone_quantity = serializers.SerializerMethodField()

    def get_phone_list(self, object):
        phone_list = PhoneNumber.objects.filter(hotel_id=object.pk)
        return PhoneNumberInitSerializer(phone_list, many=True).data

    def get_phone_quantity(self, object):
        phone_quantity = PhoneNumber.objects.filter(hotel_id=object.pk).count()
        return phone_quantity


class PhoneNumberSerializer(serializers.Serializer):
    phoneNumber = serializers.CharField()


class PhoneNumberCreateSerializer(serializers.Serializer):
    phoneNumber = serializers.CharField()
    hotel = serializers.IntegerField()

    def validate_hotel(self, value):
        hotel = value
        hotel_in_db = Hotel.objects.filter(pk=hotel)
        if not hotel_in_db.exists():
            raise serializers.ValidationError('Theres no hotel')
        phone_quantity = PhoneNumber.objects.filter(hotel_id=hotel).count()
        if phone_quantity > 2:
            raise serializers.ValidationError('Too many phone numbers for an hotel')
        return value


    def validate_phoneNumber(self, value):
        phone_number = value
        phone_number_in_db = PhoneNumber.objects.filter(phoneNumber = phone_number)
        if phone_number_in_db.exists():
            raise serializers.ValidationError("Phone number already exists")
        return value

    def savePhoneNumber(self):
        hotel = self.validated_data.get("hotel")
        phone_number = self.validated_data.get("phoneNumber")
        hotel_in_db = Hotel.objects.filter(pk=hotel).first()
        instances = PhoneNumber(phoneNumber=phone_number, hotel_id=hotel_in_db.pk)
        instances.save()
        return True


class RoomReservationSerializer(serializers.Serializer):
    room = serializers.IntegerField()
    # occupation = serializers.IntegerField()
    # price = serializers.SerializerMethodField()
    # room = serializers.IntegerField()
    # reservation = serializers.IntegerField()
    # rooms = serializers.SerializerMethodField()
    maxOccupation = serializers.SerializerMethodField()
    bedQuantity = serializers.SerializerMethodField()

    def get_maxOccupation(self, object):
        room = object["room"]
        # print(room)
        room_occupancy = Room.objects.filter(pk=room).first()
        return room_occupancy.maxOccupation

    def get_bedQuantity(self, object):
        room = object["room"]
        room_beds = Room.objects.filter(pk=room).first()
        return room_beds.bedQuantity


class RoomReservationCreateSerializer(serializers.Serializer):
    # reservation = serializers.IntegerField()
    occupation = serializers.IntegerField()
    number = serializers.IntegerField()
    maxOccupation = serializers.IntegerField()
    bedQuantity = serializers.IntegerField()
    price = serializers.IntegerField()
    hotel = serializers.IntegerField()


class BedSerializer(serializers.Serializer):
    bedQuantity = serializers.SerializerMethodField()
    def get_bedQuantity(self, object):
        print(object)
        return True

class ReservationSerializer(serializers.Serializer):
    start = serializers.DateField()
    end = serializers.DateField()
    total = serializers.IntegerField()
    # beds = serializers.SerializerMethodField()
    rooms = serializers.CharField()
    #
    # def get_beds(self, object):
    #     room = object.rooms
    #     # print(room[0])
    #     beds = Room.objects.filter(pk=room)
    #     # return beds
    #     return BedSerializer(beds, many=True).data


class ReservationCreateSerializer(serializers.Serializer):
    start = serializers.DateField()
    end = serializers.DateField()
    rooms = RoomReservationSerializer(many=True)
    occupation = serializers.IntegerField()

    def validate(self, data):
        start = data.get("start")
        end = data.get("end")
        if start > end:
            raise serializers.ValidationError('End date is not after start date.')
        rooms = data.get("rooms")
        total = 0
        max = 0
        # beds = 0
        for room in rooms:
            room_number = room["room"]
            room_object = Room.objects.filter(pk=room_number).first()
            hotel = room_object.hotel_id
        for room in rooms:
            room_number = room["room"]
            room_object = Room.objects.filter(pk=room_number).first()
            max = room_object.maxOccupation + max
            if not room_object.hotel_id == hotel:
                raise serializers.ValidationError('Rooms are not in same hotel.')
            total = room_object.price + total

        if max < data.get("occupation"):
            raise serializers.ValidationError('Too many people to stay in the rooms.')
        data["total"] = total
        # data["beds"] = beds
        return data

    def validate_end(self, value):
        now = datetime.date.today()
        end = value
        if end < now:
            raise serializers.ValidationError('This is not the future.')
        return value

    def validate_start(self, value):
        now = datetime.date.today()
        start = value
        if start < now:
            raise serializers.ValidationError('Start date has passed out.')
        return value

    def saveReservation(self):
        start = self.validated_data.get("start")
        end = self.validated_data.get("end")
        rooms = self.validated_data.get("rooms")
        total = self.validated_data.get("total")
           # serialized_rooms = RoomReservationSerializer(rooms, many=True)
        # print(serialized_rooms)
        # Reservation.objects.create(startDate, endDate, totalPrice)
        # for room in rooms:
        #     RoomReservation.objects.create(reservation=object.pk)
        # return Reservation
        # # hotel_in_db = Hotel.objects.filter(pk=hotel).first()
        instances = Reservation(rooms=rooms, start=start, end=end, total=total)
        instances.save()
        return True
        # # endDate = value
        # # rooms = data.get['rooms']
        # reservations = Reservation.objects.filter(startDate > startDate)
        # reservations_before = Reservation.objects.filter(endDate < endDate)
        # reservations.intersection(reservations, reservations_before)
        # print(reservations)


class RoomSerializer(serializers.Serializer):
    number = serializers.IntegerField()
    maxOccupation = serializers.IntegerField()
    bedQuantity = serializers.IntegerField()
    price = serializers.IntegerField()
    hotel = serializers.CharField()


class HotelRoomSerializer(serializers.Serializer):
    hotel_id = serializers.IntegerField()

class RoomCreateSerializer(serializers.Serializer):
    number = serializers.IntegerField()
    hotel = serializers.IntegerField()
    price = serializers.IntegerField()
    maxOccupation = serializers.IntegerField()
    bedQuantity = serializers.IntegerField()

    def validate_hotel(self, value):
        hotel = value
        # print(hotel)
        hotel_in_db = Hotel.objects.filter(pk=hotel)

        if not hotel_in_db.exists():
            raise serializers.ValidationError('Unknown hotel')
        return value

    def saveRoom(self, data):
        hotel = self.validated_data.get("hotel")
        price = self.validated_data.get("price")
        number = self.validated_data.get("number")
        rooms_in_db = Room.objects.filter(number=number, hotel_id=hotel).count()
        maxOccupation = self.validated_data.get("maxOccupation")
        bedQuantity = self.validated_data.get("bedQuantity")
        if rooms_in_db > 0:
            raise serializers.ValidationError('This room already exists')
        instances = Room(number=number, hotel_id=hotel, maxOccupation=maxOccupation, bedQuantity=bedQuantity, price=price)
        instances.save()
        return True
