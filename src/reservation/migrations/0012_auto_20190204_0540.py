# Generated by Django 2.1.5 on 2019-02-04 05:40

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('reservation', '0011_reservation_rooms'),
    ]

    operations = [
        migrations.AlterField(
            model_name='reservation',
            name='rooms',
            field=models.CharField(max_length=2000),
        ),
        migrations.AlterField(
            model_name='reservation',
            name='timestamp',
            field=models.DateTimeField(auto_now=True),
        ),
    ]
