# Generated by Django 2.1.5 on 2019-02-01 11:30

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('reservation', '0008_auto_20190201_0954'),
    ]

    operations = [
        migrations.RenameField(
            model_name='room',
            old_name='pricePerDay',
            new_name='number',
        ),
        migrations.RenameField(
            model_name='room',
            old_name='roomNumber',
            new_name='price',
        ),
        migrations.AlterField(
            model_name='hotel',
            name='state',
            field=models.ForeignKey(null=True, on_delete=django.db.models.deletion.SET_NULL, to='reservation.State'),
        ),
        migrations.RemoveField(
            model_name='room',
            name='hotel',
        ),
        migrations.AddField(
            model_name='room',
            name='hotel',
            field=models.ForeignKey(blank=True, default=1, on_delete=django.db.models.deletion.CASCADE, to='reservation.Hotel'),
            preserve_default=False,
        ),
    ]
